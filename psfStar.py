#! /usr/bin/env python

import sys
from numpy import median
from numpy import where
from numpy import sum as npsum
from scipy.ndimage import zoom
from astropy.io import fits
from lib.regions import load_annuli
from lib.photo import *


fitsName = sys.argv[1]
regName = sys.argv[2]


annuli = load_annuli(regName)

stars = []
centers = []
for annulus in annuli:
    annulus.recenter(fitsName)
    st = extract_star(fitsName, annulus)
    back, segmap = call_SE(st, annulus)
    stNoBack = st - back  # subtract background
    stNoBack[where(segmap==1)] = 0.0  # removing other objects
    stars.append(stNoBack)

starsEqual = equalize_shape(stars)
starsNormed = [s/npsum(s) for s in starsEqual]

zoomFactor = 4
starsZoomed = []
for star in starsNormed:
    print("star")
    starsZoomed.append(zoom(input=star, zoom=zoomFactor))

# find median values among all stars
medianStar = median(starsZoomed, axis=0)

# zoom back
psf = zoom(input=medianStar, zoom=1/zoomFactor)

# crop images to put psf exactly at center
psfCropped = recenter_psf(psf)

# finally norm psf data to 1
psfNormed = psfCropped / npsum(psfCropped)

hdu = fits.PrimaryHDU(data=psfNormed)
hdu.writeto("psf.fits", clobber=True)
