#! /usr/bin/env python

from os import path
from numpy import s_
from astropy.io import fits
from photutils.centroids import centroid_2dg


class Annulus(object):
    """ DS9 Annulus region """
    def __init__(self, xCen, yCen, r1, r2):
        xCen += 1
        yCen += 1
        self.xCen = xCen
        self.yCen = yCen
        self.r1 = r1
        self.r2 = r2
        # let's find bounding box around the annulus
        self.xStart = int(xCen - r2)
        xEnd = xCen + r2
        self.yStart = int(yCen - r2)
        yEnd = yCen + r2
        # The shape of box have to be square but let's check it just in case
        regSize = int(max(xEnd - self.xStart, yEnd - self.yStart))
        if regSize % 2 == 0:
            regSize += 1
        self.xEnd = self.xStart + regSize
        self.yEnd = self.yStart + regSize
        self.boundSlice = s_[self.yStart: self.yEnd,
                             self.xStart: self.xEnd]

    def covered_by(self, fitsName):
        ySize, xSize = fits.getdata(fitsName).shape
        if (self.xStart > 0) and (self.yStart > 0) and (self.xEnd < xSize) and (self.yEnd < ySize):
            return True
        return False
        
    def recenter(self, fitsName):
        """ Method makes adjustment to an annulus
        center to match the star position"""
        data = fits.getdata(fitsName)[self.boundSlice]
        dataSize = data.shape[0]  # Should be square so we dont need both sizes
        xCen, yCen = dataSize / 2, dataSize / 2
        # Find center of the star
        xCenFit, yCenFit = centroid_2dg(data)
        dX = xCenFit - xCen
        dY = yCenFit - yCen
        # Change ring parameters according to that center:
        self.__init__(self.xCen+dX, self.yCen+dY, self.r1, self.r2)

        

def load_annuli(regFileName):
    """ Function reads ds9 region file and returns list
    of Annulus objects"""
    if not path.exists(regFileName):
        print("File %s not found" % regFileName)
        return []
    annuli = []
    for line in open(regFileName):
        if line.startswith("annulus"):
            values = [float(v) for v in line.strip()[8:-1].split(",")]
            annuli.append(Annulus(*values))
    return annuli
