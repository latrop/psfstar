#! /usr/bin/env python

import itertools
import subprocess
from os import path
from os import remove
from numpy import nan
from numpy import hypot
from numpy import array
from numpy import where
from numpy import sum as npsum
from numpy import pad
from numpy import zeros_like
from scipy.ndimage import maximum_position
from astropy.io import fits
from photutils.centroids import centroid_2dg


def estimate_background(fitsName, annulus):
    """ Function estimates main value of flux inside of
    given annulus"""
    data = fits.getdata(fitsName)

    if annulus.covered_by(fitsName):
        subImage = data[annulus.boundSlice]
    else:
        print("Annulus goes out of image edge.")
        return nan
    
    # Find a pixels inside of ring (i.e. between r1 and r2 circles)
    ySizeSub, xSizeSub = subImage.shape
    xCenSub, yCenSub = (xSizeSub-1)/2, (ySizeSub-1)/2
    iTotal = 0.0
    numOfPix = 0
    for i in range(xSizeSub):                             ## TODO: do this in a numpy way!
        for j in range(ySizeSub):
            d = hypot(i-xCenSub, j-yCenSub)
            if (d > annulus.r1) and (d < annulus.r2):
                iTotal += subImage[j, i]
                numOfPix += 1

    # Return mean intensity inside annulus
    return iTotal / numOfPix


def extract_star(fitsName, annulus):
    """ Function extract rectangular area that bounds given annulus.
    Returns: data array"""
    data = fits.getdata(fitsName)
    if annulus.covered_by(fitsName):
        return data[annulus.boundSlice]
    else:
        print("Annulus goes out of image edge.")
        return nan


def equalize_shape(listOfStars):
    maxSize = max([s.shape[0] for s in listOfStars])
    outStars = []
    for star in listOfStars:
        size = star.shape[0]
        padValue = int((maxSize - size) / 2)
        if padValue > 0:
            paddedStar = pad(star, pad_width=padValue, mode="constant",
                             constant_values = 0)
            outStars.append(paddedStar)
        else:
            outStars.append(star)
    return outStars


class SExCatalogue(object):
    def __init__(self, catFileName):
        self.objectList = []
        self.legend = []
        self.inds = [-1]
        self.index = 0  # For iterations
        # Parse SE catalogue with arbitrary number of objects and parameters
        for line in open(catFileName):
            sLine = line.strip()
            obj = {}
            if sLine.startswith("#"):
                # legend line
                self.legend.append(sLine.split()[2])
                self.inds.insert(-1, int(sLine.split()[1]) - 1)
                continue
            params = [float(p) for p in line.split()]
            for i in range(len(self.legend)):
                b = self.inds[i]
                e = self.inds[i+1]
                if e == b + 1:
                    obj[self.legend[i]] = params[b]
                else:
                    obj[self.legend[i]] = params[b:e]
            self.objectList.append(obj)
        self.numOfObjects = len(self.objectList)
    
    def find_nearest(self, x, y):
        """ Returns nearest object to given coordinates"""
        nearest = min(self.objectList, key=lambda obj: hypot(x-obj["X_IMAGE"], y-obj["Y_IMAGE"]))
        dist = hypot(x-nearest["X_IMAGE"], y-nearest["Y_IMAGE"])
        if dist < 3.0:
            return nearest
        else:
            return None

    def get_obj_with_max_value(self, param):
        idx = maximum_position([obj[param] for obj in self.objectList])[0]
        return self.objectList[idx]

    def get_median_value(self, parameter):
        return np.median([obj[parameter] for obj in self.objectList])

    def get_all_values(self, parameter):
        return np.array([obj[parameter] for obj in self.objectList])
    

    def __iter__(self):
        return self

    def next(self):
        """ Iteration method """
        if self.index < self.numOfObjects:
            self.index += 1
            return self.objectList[self.index-1]
        else:
            self.index = 1
            raise StopIteration


def call_SE(data, annulus):
    """ Function calls SExtractor for given data """
    # At first save data as a fits file
    hdu = fits.PrimaryHDU(data=data)
    ySize, xSize = data.shape
    xCen = xSize / 2
    yCen = ySize / 2
    hdu.writeto("temporary.fits", clobber=True)

    # Create a weight map, where good pixels are only
    # inside of the annulus (between r1 and r2)
    # weights = zeros_like(data)
    # for i in range(xSize):                             ## TODO: do this in a numpy way!
    #     for j in range(ySize):
    #         d = hypot(i-xCen, j-yCen)
    #         if (d > annulus.r1) and (d < annulus.r2):
    #             weights[j, i] = 1
    # hdu = fits.PrimaryHDU(data=weights)
    # hdu.writeto("weights.fits", clobber=True)
    
    pathToSEConfig = path.join("lib", "default.sex")
    callString = " ".join(["sex", "temporary.fits", '-c %s ' % pathToSEConfig, "-VERBOSE_TYPE QUIET"])
    subprocess.call(callString, shell=True)

    # load background data
    backData = fits.getdata("background.fits")

    # load segmentation map. We are anterested in all objects
    # except of our main star
    cat = SExCatalogue("field.cat")
    segmap = fits.getdata("seg.fits")  # sextract resulting segmap
    outSegmap = zeros_like(data)  # our segmap without main star
    if cat.numOfObjects > 1:
        starObj = cat.get_obj_with_max_value("FLUX_AUTO")
        for obj in cat.objectList:
            objNum = int(obj["NUMBER"])
            if starObj["NUMBER"] != objNum:
                outSegmap[where(segmap == objNum)] = 1

    remove("background.fits")
    remove("temporary.fits")
    remove("seg.fits")
    remove("field.cat")
    return backData, outSegmap


def recenter_psf(data):
    dataSize = data.shape[0]
    xCenFit, yCenFit = centroid_2dg(data)
    xCenFit = int(xCenFit)
    yCenFit = int(yCenFit)
    outSize = int(min(xCenFit, dataSize-xCenFit, yCenFit, dataSize-yCenFit))
    outData = data[yCenFit-outSize+1:yCenFit+outSize+2, xCenFit-outSize+1:xCenFit+outSize+2]
    return outData
